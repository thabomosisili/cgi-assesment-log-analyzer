package com.cgi.log.analyser.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cgi.log.analyser.data.model.LogEntry;


public class LogParser implements LogParserService {

	@Override
	public LogEntry parserLogEntry(String logEntryLine) {
		
		String logEntryPattern = "(\\S+) (\\S+) (DEBUG|WARN|INFO|ERROR) (\\S+)";
		Pattern p = Pattern.compile(logEntryPattern);
		Matcher matcher = p.matcher(logEntryLine);
		if (!matcher.find() || matcher.groupCount() == 0) {
			System.err.println("Bad log entry (or problem with RE?):");
			return null;
		}
		
		/*
		 * logEntry.setDate(matcher.group(1)); logEntry.setTime(matcher.group(2));
		 * logEntry.setType(matcher.group(3)); logEntry.setThread(matcher.group(4));
		 */
	//	logEntry.setDescription(logEntryLine.substring(matcher.end()));
		 
		
		System.out.println("Date:" + matcher.group(1));
		System.out.println("Time: " + matcher.group(2));
		System.out.println("TYPE: " + matcher.group(3));
		System.out.println("THREAD: " + matcher.group(4));
		System.out.println("DESCRIPTION: " + logEntryLine.substring(matcher.end()) );
		return parserLogEntry(null);
	}

	@Override
	public LogEntry parserLogEntry(String logEntryLine, LogEntry logEntry) {
		// TODO Auto-generated method stub
		return null;
	}


}
