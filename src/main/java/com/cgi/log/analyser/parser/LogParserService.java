package com.cgi.log.analyser.parser;

import com.cgi.log.analyser.data.model.LogEntry;

public interface LogParserService {
	

	LogEntry parserLogEntry(String logEntryLine, LogEntry logEntry);

	LogEntry parserLogEntry(String logEntryLine);

}
