package com.cgi.log.analyser.data.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.cgi.log.analyser.data.exception.LogEntryNotFoundException;
import com.cgi.log.analyser.data.model.LogEntry;
import com.cgi.log.analyser.data.repo.LogEntryRepository;

@Service
@Transactional
public class LogEntryService {
	
	private final LogEntryRepository logEntryRepository;
	
	public LogEntryService( LogEntryRepository logEntryRepository) {
		this.logEntryRepository = logEntryRepository;
	}
	
	public LogEntry findLogEntryById(Long id) {
        return logEntryRepository.findLogEntryById(id)
                .orElseThrow(() -> new LogEntryNotFoundException("Log entry by id " + id + " was not found"));
    }

	  public LogEntry addLogEntry (LogEntry logEntry) {
	        return logEntryRepository.save(logEntry);
	    }

	    public List<LogEntry> findAllLogEntries() {
	        return logEntryRepository.findAll();
	    }
	    
	    public   Optional<List<?>> sortByLogTypeFrequency (String errorType){
			  return Optional.of(logEntryRepository.sortByLogTypeFrequency(errorType));
	    }
	    
	    public  Optional<List<LogEntry>> findByLogType(String errorType){
	    	 return Optional.of(logEntryRepository.findByLogType(errorType));
	    }
	    
	    @Transactional
		public List<LogEntry> saveAllLogsEntries(List<LogEntry> logEntryList) {
			return logEntryRepository.saveAll(logEntryList);
		}
}
