package com.cgi.log.analyser.file.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cgi.log.analyser.data.model.LogEntry;
import com.cgi.log.analyser.data.repo.LogEntryRepository;
import com.cgi.log.analyser.data.service.LogEntryService;
import com.cgi.log.analyser.parser.LogParserService;
@Component
public class FileReader implements FileReaderService {

	private final LogEntryService logEntryService;

	@Autowired
	public FileReader( LogEntryService logEntryService) {
		this.logEntryService = logEntryService;
	}

	public void fileProcessor(Path source) throws IOException {
		List <LogEntry> logEntries = new ArrayList<LogEntry>();
		try (InputStream inputStream = Files.newInputStream(source);
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
				Stream<String> linesStream = bufferedReader.lines();
				Scanner sc = new Scanner(inputStream, "UTF-8");
				) {
			
			while (sc.hasNextLine()) {
		        String line = sc.nextLine();
				String logEntryPattern = "(\\S+) (\\S+) ([A-Z]*) (\\S+)";
				Pattern p = Pattern.compile(logEntryPattern);
				Matcher matcher = p.matcher(line);
				if (!matcher.find() || matcher.groupCount() == 0) {
					continue;
				}
				LogEntry logEntry = new LogEntry(matcher.group(1),matcher.group(2),matcher.group(3), matcher.group(4),line.substring(matcher.end()));
				
				logEntries.add(logEntry);
				
		    }
			logEntryService.saveAllLogsEntries(logEntries);
		}
	}
	
public LogEntry parserLogEntry(String logEntryLine) {
	    LogEntry logEntry = new LogEntry();
		String logEntryPattern = "(\\S+) (\\S+) (DEBUG|WARN|INFO|ERROR) (\\S+)";
		Pattern p = Pattern.compile(logEntryPattern);
		Matcher matcher = p.matcher(logEntryLine);
		if (!matcher.find() || matcher.groupCount() == 0) {
			System.err.println("Bad log entry (or problem with RE?):");
			return null;
		}
	 
	//logEntry.setId(logEntryLine.substring(matcher.end()));
	logEntry = new LogEntry(matcher.group(1),matcher.group(2),matcher.group(3), matcher.group(4),logEntryLine.substring(matcher.end()));
//	logEntryService.addLogEntry(logEntry);
		
		System.out.println("Date:" + matcher.group(1));
		System.out.println("Time: " + matcher.group(2));
		System.out.println("TYPE: " + matcher.group(3));
		System.out.println("THREAD: " + matcher.group(4));
		System.out.println("DESCRIPTION: " + logEntryLine.substring(matcher.end()) );
		return logEntry;
	}

}
