package com.cgi.log.analyser.parser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.context.junit4.SpringRunner;

import com.cgi.log.analyser.data.model.LogEntry;

import org.springframework.boot.test.context.SpringBootTest;

@AutoConfigureMockMvc
@SpringBootTest
@RunWith(SpringRunner.class)
public class LogParserTest {
	

	
	@Test
	public void testParserLogEntry_Pass() throws Exception {
		String logEntryLine = "2018-09-10 15:32:41,709 DEBUG [main] AsyncConfiguration: Creating Async Task Executor";
		LogParser logParser = new LogParser();
		logParser.parserLogEntry(logEntryLine);
		
	}
	@Test
	public void testParserLogEntry_Fail() throws Exception {
		String logEntryLine = "at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:241) [tomcat-embed-core-7.0.56.jar:7.0.56]";
		LogParser logParser = new LogParser();
		logParser.parserLogEntry(logEntryLine);
		
	}
	

}
